package com.example.alen.testtest;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private RelativeLayout startGameHolder;
    private RelativeLayout gameHolder;
    private TextView bestScore;
    private EditText numberInput;

    private int randomNumber;
    private int bestScoreValue = 0;
    private int score;
    private boolean firstTime = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        startGameHolder = (RelativeLayout) findViewById(R.id.score_holder);
        gameHolder = (RelativeLayout) findViewById(R.id.game_holder);
        bestScore = (TextView) findViewById(R.id.best_score);
        numberInput = (EditText) findViewById(R.id.picked_number);
    }

    public void startNewGame(View view) {
        score = 0;
        startGameHolder.setVisibility(View.GONE);
        gameHolder.setVisibility(View.VISIBLE);

        Random rand = new Random();
        randomNumber = rand.nextInt(100) + 1;
    }

    public void checkNumber(View view) {
        if(!numberInput.getText().toString().isEmpty()) {
            score ++;
            int currentNumber = Integer.valueOf(numberInput.getText().toString());

            if(currentNumber > randomNumber) {
                Toast.makeText(this, "Your number is smaller!", Toast.LENGTH_SHORT).show();
            } else if(currentNumber < randomNumber) {
                Toast.makeText(this, "Your number is bigger!", Toast.LENGTH_SHORT).show();
            } else if(currentNumber == randomNumber) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("You WIN");
                builder.setPositiveButton("NEW GAME", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        numberInput.setText("");
                        gameHolder.setVisibility(View.GONE);
                        startGameHolder.setVisibility(View.VISIBLE);
                        if(Integer.valueOf(bestScore.getText().toString()) > score || firstTime) {
                            bestScore.setText(String.valueOf(score));
                            firstTime = false;
                        }
                    }
                });
                builder.setMessage("Congratulation, you pick the correct number. Best luck in next game!");

                // create alert dialog
                AlertDialog alertDialog = builder.create();

                // show it
                alertDialog.show();
            }
        } else {
            Toast.makeText(this, "Please pick a number!", Toast.LENGTH_SHORT).show();
        }

    }
}
